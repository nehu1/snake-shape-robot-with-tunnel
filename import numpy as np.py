import numpy as np
import mujoco_py
import os
import time

record=0
distance=0

def set_motor(time = 0.0, frequency = 2.0):
    joint_num = 6
    control_tork = list(np.zeros(joint_num))
    for j in range(joint_num):
        control_tork[j] = np.pi/4*np.sin(np.pi/frequency*time+np.pi/2*j+np.pi/2)
    return control_tork
    
def moving(distance = 0.0):
    global record
    if distance<record:
        record = distance
        
if __name__ == "__main__":
    #planar_snake_cars_servo.xml
    #swimmer.xml
    #+-0.28 most narrow to +-0.40 most wide
    model = mujoco_py.load_model_from_path('/home/neili/ece215/final/swimmer.xml')
    sim = mujoco_py.MjSim(model)
    viewer = mujoco_py.MjViewer(sim)
    current_time = 0
    record_x_1=np.array([0.,0.,0.,0.,0.,0.,0.])
    record_x_2=np.array([0.,0.,0.,0.,0.,0.,0.])
    record_y_1=np.array([0.,0.,0.,0.,0.,0.,0.])
    record_y_2=np.array([0.,0.,0.,0.,0.,0.,0.])
    for m in range(4):
        for k in range(3):
            fre=1.3-0.1*k-0.3*m  #should less than 1.3
            for i in range(4000-1000*m):
                sim.data.ctrl[:] = set_motor(current_time, fre)
                sim.step()# update simulator
                for k in range(7):#calculate total moving distance
                    record_x_1[k]=np.abs(sim.data.body_xpos[3+k][0])
                    record_y_1[k]=np.abs(sim.data.body_xpos[3+k][1])
                    distance+=((record_x_1[k]-record_x_2[k])**2+(record_y_1[k]-record_y_2[k])**2)**0.5
                    record_x_2[k]=record_x_1[k]
                    record_y_2[k]=record_y_1[k] 
                viewer.render()
                current_time+=0.0125     #time update
                moving(record_x_1[k])# get real distance
            print(distance)
            sim.reset()
            current_time = 0
            record=0
            distance=0